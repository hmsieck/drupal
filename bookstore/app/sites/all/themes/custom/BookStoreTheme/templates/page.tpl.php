<div class="site-container">
    
    <!-- header -->
    <div class="header-wrapper">
    </div>
    
    <!-- site title -->
    <div class="site-title-container">
        <div class="site-title">
            <p>Book Store</p>
        </div>
    </div>
    
    <!-- content -->
    <div class="content-container container">
        <div class="menu-container inner-container">
            <?php 
                print theme('links',array('links'=>$main_menu));
            ?>
        </div>
        
        <!-- tabs will go here -->
        <div class="tab-container container">
            <?php if ($tabs): ?>
                  <?php print render($tabs); ?>
            <?php endif; ?>
        </div>
        
        <div class="content inner-container clearfix">
                    
                  <?php print render($page['content']); ?>
                </div>
            </div>
            
    
    <!-- footer -->
    <div class="footer-container container">
        <?php if($page['footer']): ?>
            <div class="footer-content inner-container">
                <?php print render($page['footer']); ?>
            </div>
        <?php endif; ?>
    </div>
    
</div>
