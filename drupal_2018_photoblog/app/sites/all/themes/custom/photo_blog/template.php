<?php
  
function photo_blog_preprocess_page(&$variables){
  
  $body_class = "full-width";
  $leftcolumn_class = "";
  $rightcolumn_class = "";
  $page = $variables["page"];
  
  if(!empty($page["right_column"])){
    $rightcolumn_class = "one-fourth";
    $body_class = "three-fourths";
  }
	
  if(!empty($page["left_column"])){
    $leftcolumn_class = "one-fourth";
    $body_class = "three-fourths";
  }
  
  
  $variables["photo_blog"]["body_class"] = $body_class;
  $variables["photo_blog"]["leftcolumn_class"] = $leftcolumn_class;
  $variables["photo_blog"]["rightcolumn_class"] = $rightcolumn_class;
  
}